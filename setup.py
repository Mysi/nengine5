from setuptools import setup, find_packages, Extension, Command
from setuptools.command import build_py

from ofs2 import *
from ofs2 import ofs2_ngn

collision = Extension("collision", ["src/c/collision.c"])


def pack_ofs(validate=True):
    ofsbuilder = OFSFileBuilder()

    if validate:
        ofsbuilder.add_validator(ofs2_ngn.ConfigFileValidator())

    ofsbuilder.relpath_start = os.path.dirname(os.path.abspath(__file__))

    for path, dirs, files in os.walk("data"):
        for file in files:
            ofsbuilder.add(path + "/" + file)
            print(path + "/" + file)

    ofsbuilder.write_out("data.ofs2")


class BuildPyCommand(build_py.build_py):
    """Custom build command."""

    def run(self):
        print("Packing OFS")

        pack_ofs()

        print("OFS Packed")

        build_py.build_py.run(self)


class PackOfsCmd(Command):
    description = 'pack the data into OFS'
    user_options = [
        # The format is (long option, short option, description).
        ('skip-validators', "s", 'skip validators'),
    ]

    def initialize_options(self):
        """Set default values for options."""
        # Each user option must be listed here with their default value.
        self.skip_validators = False

    def finalize_options(self):
        ...

    def run(self):
        pack_ofs(False if self.skip_validators else True)
        print("Finished")



setup(
    cmdclass={
        'build_py': BuildPyCommand,
        'pack_ofs': PackOfsCmd,
    },
    name='ngn4',
    version='0.0.1',
    packages=['ofs2'],
    install_requires=['pyglet', 'pypresence'],
    url='https://ertego.herokuapp.com/',
    license='LGPL',
    author='The Ertego Team',
    author_email='',
    description='',
    scripts=['ngn5.py'],
    entry_points={
            'gui_scripts': [
                'run = ngn5:main',
            ]
        },
    data_files = [('', ["data.ofs2", 'utils.py'])],
    include_package_data=True,
    zip_safe=False,
    test_suite='tests.TestOFS',
    ext_modules=[collision]

)
