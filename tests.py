import os
import unittest
import tempfile

import ofs2
import ofs2.ofs2_ngn
import random

COUNT = 5


class TestOFS(unittest.TestCase):
    def setUp(self):
        self.datas = [bytes(random.getrandbits(8) for _ in range(200)) for _ in range(COUNT)]
        self.ofs_builder = ofs2.OFSFileBuilder()

        self.temp_data_files = []

        self.ofs_builder.relpath_start = "./"

        for i in range(COUNT):
            temp_data_file = tempfile.NamedTemporaryFile("wb")
            temp_data_file.write(self.datas[i])
            temp_data_file.flush()

            self.ofs_builder.add(path=temp_data_file.name)
            self.temp_data_files.append(temp_data_file)

        self.temp_ofs_output = tempfile.NamedTemporaryFile()
        self.ofs_builder.write_out(self.temp_ofs_output.name)

        self.ofs = ofs2.OFSFile(self.temp_ofs_output.name)

    def test_header_filecount(self):
        self.assertEqual(COUNT, len(self.ofs.extract_file_list()))

    def test_invalid_file_build(self):
        self.assertRaises(FileNotFoundError, lambda: ofs2.OFSFileBuilder().add("/rainbowland/????"))

    def test_datas(self):
        for i in range(COUNT):
            self.assertEqual(self.datas[i], self.ofs.get_file(os.path.relpath(self.temp_data_files[i].name, "./")).read())

    def tearDown(self):
        self.ofs.input.close()
        for file_ in self.temp_data_files:
            file_.close()
        self.temp_ofs_output.close()


if __name__ == '__main__':
    unittest.main()
