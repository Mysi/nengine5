#include <Python.h>
#define max(a,b) (((a) > (b)) ? (a) : (b))
#define min(a,b) (((a) < (b)) ? (a) : (b))

/*
 * Implements an the collision function.
 */
PyDoc_STRVAR(collision_example_doc, "are_colliding(collision_list_1, collision_list_2)\
\
Compares two lists of lines (lists out of begin (list out of x and y) and end of a line) and returns True if they're colliding");

PyObject *collision_are_colliding(PyObject *self, PyObject *args) {
    PyObject * list_a = NULL;
    PyObject * list_b = NULL;
	int size_a = 0;
	int size_b = 0;

	if (!PyArg_ParseTuple(args, "OO", &list_a, &list_b)) {
		return NULL;
	}

	size_a = PyList_Size(list_a);
	size_b = PyList_Size(list_b);

	if (size_a < 0 || size_b < 0) {
		PyErr_BadArgument();
		return NULL;
	}

	PyObject * line_a = NULL;
	PyObject * line_b = NULL;

	/*
	calculates the functions of the lines, then calculates the point of intersection
	and looks if the point is inside the sector of the lines that we want to check
	*/

	// the coordinates of the beginning and the end points
	int start1[2];
	int end1[2];
	int start2[2];
	int end2[2];

	// the gradients of the functions
	// y = (m) * x + t
	double m1;
	double m2;

	// the y-coordinate of the intersection of the function with the y-axis
	// y = m * x + (t)
	double t1;
	double t2;

	// the coordinates of the intersection
	double x;
	double y;

	int ii = 0;
	for (int i = 0; i < size_a; i++) {
		line_a = PyList_GetItem(list_a, i);

		if (PyList_Size(line_a) != 2) {
			PyErr_BadArgument();
			return NULL;
		}

		start1[0] = PyLong_AsLong(PyList_GetItem(PyList_GetItem(line_a, 0), 0));
		start1[1] = PyLong_AsLong(PyList_GetItem(PyList_GetItem(line_a, 0), 1));
		end1[0] = PyLong_AsLong(PyList_GetItem(PyList_GetItem(line_a, 1), 0));
		end1[1] = PyLong_AsLong(PyList_GetItem(PyList_GetItem(line_a, 1), 1));

		int delta_x = end1[0] - start1[0];
		if (delta_x == 0) {
			m1 = 99999999.0;
		}
		else {
			m1 = (double)(end1[1] - start1[1]) / (double)(delta_x);
		}

		t1 = start1[1] - (m1 * start1[0]);

		for (; ii < size_b; ii++) {
			line_b = PyList_GetItem(list_b, ii);

			if (PyList_Size(line_b) != 2) {
				PyErr_BadArgument();
				return NULL;
			}

			start2[0] = PyLong_AsLong(PyList_GetItem(PyList_GetItem(line_b, 0), 0));
			start2[1] = PyLong_AsLong(PyList_GetItem(PyList_GetItem(line_b, 0), 1));
			end2[0] = PyLong_AsLong(PyList_GetItem(PyList_GetItem(line_b, 1), 0));
			end2[1] = PyLong_AsLong(PyList_GetItem(PyList_GetItem(line_b, 1), 1));

			delta_x = end2[0] - start2[0];
			if (delta_x == 0) {
				m2 = 99999999.0;
			}
			else {
				m2 = (double)(end2[1] - start2[1]) / (double)(delta_x);
			}

			t2 = start2[1] - (m2 * start2[0]);

			if (m1 == m2) { // both lines are parallel
				if (t2 == t1) {  // they have  the same function
					if ((min(start1[0], end1[0]) <= max(start2[0], end2[0])) && (min(start1[0], end1[0]) >= min(start2[0], end2[0]))) {
						if ((min(start1[1], end1[1]) <= max(start2[1], end2[1])) && (min(start1[1], end1[1]) >= min(start2[1], end2[1]))) {
							return Py_True;
						}
					}
				}
				continue;
			}
			x = (t2 - t1) / (m1 - m2);
			y = m1 * x + t1;

			if (min(start1[0], end1[0]) <= x && x <= max(start1[0], end1[0])) {
				if (min(start2[0], end2[0]) <= x && x <= max(start2[0], end2[0])) {
					if (min(start1[1], end1[1]) <= y && y <= max(start1[1], end1[1])) {
						if (min(start2[1], end2[1]) <= y && y <= max(start2[1], end2[1])) {
							return Py_True;
						}
					}
				}
			}
		}
	}

    return Py_False;
}

/*
 * List of functions to add to collision in exec_collision().
 */
static PyMethodDef collision_functions[] = {
    { "are_colliding", (PyCFunction)collision_are_colliding, METH_VARARGS | METH_KEYWORDS, collision_example_doc },
    { NULL, NULL, 0, NULL } /* marks end of array */
};

/*
 * Initialize collision. May be called multiple times, so avoid
 * using static state.
 */
int exec_collision(PyObject *module) {
    PyModule_AddFunctions(module, collision_functions);

    PyModule_AddStringConstant(module, "__author__", "Marwin Kreuzig");
    PyModule_AddStringConstant(module, "__version__", "1.0.0");
    PyModule_AddIntConstant(module, "year", 2018);

    return 0; /* success */
}

/*
 * Documentation for collision.
 */
PyDoc_STRVAR(collision_doc, "The collision module");


static PyModuleDef_Slot collision_slots[] = {
    { Py_mod_exec, exec_collision },
    { 0, NULL }
};

static PyModuleDef collision_def = {
    PyModuleDef_HEAD_INIT,
    "collision",
    collision_doc,
    0,              /* m_size */
    NULL,           /* m_methods */
    collision_slots,
    NULL,           /* m_traverse */
    NULL,           /* m_clear */
    NULL,           /* m_free */
};

PyMODINIT_FUNC PyInit_collision() {
    return PyModuleDef_Init(&collision_def);
}
