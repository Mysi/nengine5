from distutils.core import setup, Extension

module1 = Extension('collision',
                    sources=['collision.c'])

setup(name='ngn5collision',
      version='1.0',
      description='nEngine5 Collision',
      ext_modules=[module1])
