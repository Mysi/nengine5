import abc
import os
import io
import sys
import zlib


def discover_ofs(filename):
    if os.path.isfile(os.path.dirname(os.path.dirname(__file__)) + os.sep + filename):
        return os.path.dirname(os.path.dirname(__file__)) + os.sep + filename


class OFSFileValidationError(Exception):
    """Error raised if an ofs file validation fails"""
    def __init__(self, reason, *args, **kwargs):
        super(OFSFileValidationError, self).__init__(*args, **kwargs)
        self.reason = reason


class OFSFileValidator(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def validate_file(self, path: str):
        """
        Validate a file
        :param path: the path to the file to be validated, e.g. '/data/file.json'
        :return: True if valid
        :raises: OFSFileValidationError
        """


class OFSFile(object):
    def __init__(self, path):
        self.path = path
        self.input = open(self.path, "rb")
        self.index_length, self.index_length_indicator_length = self.get_index_length()
        self.base_offset = self.index_length + self.index_length_indicator_length

        self._cached_index = None

        self.variables = self.extract_index_variables()
        if self.variables[b"OFS_VERSION"] != b"2":
            raise NotImplementedError("OFS Version mismatch")

        if self.variables[b"VALIDATIONS_PASSED"] != b"True":
            print("Loading unclean file! Please make sure all validations passed during build", file=sys.stderr)

    def get_index_length(self, try_size=100):
        self.input.seek(0)
        content = self.input.read(try_size)
        if b"\n" not in content:
            print(f"No Index length indicator found, retrying with {try_size*10} bytes")
            return self.get_index_length(try_size*10)

        length = content[:content.find(b"\n")]

        if not length.isdigit():
            raise ValueError("Not a valid OFS2 file")

        return int(length), len(length) + 1  # +1 to compensate the newline after the length indicator

    def extract_index(self):
        if self._cached_index:
            return self._cached_index
        else:
            self.input.seek(self.index_length_indicator_length)
            index = self.input.read(self.index_length)
            self._cached_index = index
            return index

    def extract_index_variables(self):
        index = self.extract_index()
        variables = [line for line in index.split(b"\n") if line.startswith(b"?")]
        result = {var.split(b"=")[0][1:]: var.split(b"=")[1] for var in variables}
        return result

    def extract_file_list(self):
        index = self.extract_index()
        split = [line.split(b"?") for line in index.split(b"\n") if not line.startswith(b"?")]
        return {str(line[0], encoding="utf8"): (int(line[1]), int(line[2])) for line in split[:-1]}  # :-1 becasue the last entry is empty

    def get_file_info(self, path):
        return self.extract_file_list()[path]

    def get_raw_file(self, path):
        start, length = self.get_file_info(path)
        self.input.seek(self.base_offset + start)
        return self.input.read(length)

    def get_file(self, path):
        return io.BytesIO(self.get_raw_file(path))


class OFSBuilderFile(object):
    def __init__(self, path, size):
        self.path = path
        self.size = size


class OFSFileBuilder(object):
    def __init__(self):
        self.files = []
        self.current_offset = 0
        self.relpath_start = os.path.dirname(__file__)
        self.validators = []

    def add_validator(self, validator):
        if not isinstance(validator, OFSFileValidator):
            raise TypeError("No OFSFileValidator")
        self.validators.append(validator)

    def add(self, path):
        if not os.path.isfile(path):
            raise FileNotFoundError
        self.files.append(OFSBuilderFile(path, os.path.getsize(path)))

    def generate_index(self, **kwargs):
        result = str()
        result += "?OFS_VERSION=2\n"
        result += "?FILE_COUNT={}\n".format(len(self.files))

        for key, val in kwargs.items():
            result += f"?{key}={str(val)}\n"

        file_strs = []
        current_offset = 0
        for file_ in self.files:
            relative_path = os.path.relpath(file_.path, self.relpath_start)
            file_strs.append(f"{relative_path}?{current_offset}?{file_.size}")
            current_offset += file_.size

        result += "\n".join(file_strs) + "\n"

        return bytes(result, "utf8")

    def write_out(self, path):
        validations_passed = True
        for file_ in self.files:
            for validator in self.validators:  # type: OFSFileValidator
                try:
                    validator.validate_file(file_.path)
                except OFSFileValidationError as error:
                    validations_passed = False
                    print(f"Validation for {file_.path} failed\n\t-->\t{error.reason}", file=sys.stderr)

        write_offset = 0
        output = open(path, "wb")
        index = self.generate_index(VALIDATIONS_PASSED=validations_passed)
        output.write(bytes(str(len(index)) + "\n", "utf8"))
        output.write(index)
        write_offset = index.__sizeof__()
        for file_ in self.files:
            input_ = open(file_.path, "rb")
            output.write(input_.read())
            input_.close()
        output.close()


if __name__ == '__main__':
    builder = OFSFileBuilder()
    builder.add("./main.py")
    builder.add("./data/crashed.png")
    builder.write_out("data.ofs2")

    ofs = OFSFile("data.ofs2")
    print(ofs.extract_index())
    print(ofs.extract_index_variables())
    print(ofs.extract_file_list())
    print(ofs.get_file_info("main.py"))
    print(ofs.get_file("main.py").read())
    print(ofs.get_file("data/crashed.png").read())
