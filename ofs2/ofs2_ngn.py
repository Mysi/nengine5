import json

from ofs2 import *


# code borrowed from https://stackoverflow.com/a/287944
class BColors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def _split_folders(path):
    # code borrowed from https://stackoverflow.com/a/3167684
    folders = []
    while 1:
        path, folder = os.path.split(path)

        if folder != "":
            folders.append(folder)
        else:
            if path != "":
                folders.append(path)

            break

    folders.reverse()

    return folders


class ConfigValidatorStr:
    def __init__(self, notnull=False):
        self.notnull = notnull

    def validate(self, key, value):
        if type(value) != str:
            raise OFSFileValidationError(f"The value of {key} is {type(value)} while the template requires a string")

        if self.notnull and not value:
            raise OFSFileValidationError(f"value of {key} is null while the template does not allow that")

        return True


class ConfigValidatorInt:
    def __init__(self, range_=None, str_length=None):
        self.range = range_
        self.length = str_length

    def validate(self, key, value):
        if type(value) != int:
            raise OFSFileValidationError(f"The value of {key} is {type(value)} while the template requires an integer")

        if self.range:
            if not self.range[0] <= value <= self.range[1]:
                raise OFSFileValidationError(
                    f"value of {key} is not in the allowed range between {self.range[0]} and {self.range[1]}")

        if self.length:
            if len(str(value)) != self.length:
                raise OFSFileValidationError(
                    f"Value of {key} does not match the required length of {self.length} characters")

        return True


class ConfigFileValidator(OFSFileValidator):
    templates = {"str": ConfigValidatorStr, "int": ConfigValidatorInt}

    def validate_config(self, template: dict, config: dict):
        for key, val in template.items():
            if key not in config:
                raise OFSFileValidationError("Missing config parameter " + key)

            if type(val) == dict:
                self.validate_config(template[key], config[key])
            else:
                template = val
                value = config[key]

                if not eval(template, self.templates).validate(key, value):
                    raise OFSFileValidationError("Validation Failed")

    def validate_file(self, path: str):
        folders = _split_folders(path)

        if folders[0] != "data":
            print(BColors.WARNING + "[ConfigFileValidator] Packing file outside of data/ ? skipping..." + BColors.ENDC)
            return None

        template_path = folders[:]
        template_path[0] = "data_origin"
        template_filename = template_path[-1]
        template_filename = template_filename.split(".")
        template_filename.insert(-1, "tmpl")
        template_path[-1] = ".".join(template_filename)
        template_path = os.sep.join(template_path)

        if os.path.isfile(template_path):
            return self.validate_config(json.load(open(template_path)), json.load(open(path)))


if __name__ == '__main__':
    builder = OFSFileBuilder()
    builder.add_validator(ConfigFileValidator())
    builder.add("main.py")
    builder.add("data/config/nengine.json")
    builder.write_out("data_dbg.ofs2")

    ofs = OFSFile("data_dbg.ofs2")
    print(ofs.extract_index())
    print(ofs.extract_index_variables())
    print(ofs.extract_file_list())
