from ofs2.ofs2_ngn import *

if __name__ == '__main__':
    builder = OFSFileBuilder()

    for path, dirs, files in os.walk("data"):
        for file in files:
            builder.add(path + "/" + file)

    builder.write_out("data.ofs2")
