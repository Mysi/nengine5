import gettext
import inspect
import os
import re


LOCALEDIR = os.environ.get("NGN_ROOT", os.path.dirname(__file__)) + os.sep + "/locales"


NGN_ROOT = os.environ.get("NGN_ROOT", os.path.dirname(__file__)) + os.sep


def get_domain(path):
    return "-".join(os.path.relpath(path, NGN_ROOT + "data").split(os.sep))


class GetTextWrapper:
    def __init__(self, domain):
        self.gettext = gettext.translation(domain, LOCALEDIR, self.get_language())

    @staticmethod
    def get_language():
        return [os.environ.get("LANG", "en_EN"), "en_EN"]

    def __call__(self, *args, **kwargs):
        return self.gettext.gettext(*args, **kwargs)


if __name__ == '__main__':
    print(get_domain(NGN_ROOT + "data/config/test.py"))
