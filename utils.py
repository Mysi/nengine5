import json
import os
import sys
import time
from typing import Optional

from pyglet.image import Animation, AnimationFrame
import pyglet
import pypresence

from ofs2 import OFSFile, discover_ofs

NGN_DEBUG = os.environ.get("NGN_DEBUG", False)

ofs = None
if discover_ofs("data.ofs2") and not NGN_DEBUG:
    ofs = OFSFile(discover_ofs("data.ofs2"))


class OnScreenObject(object):
    """
    Abstracting both visible and invisible sprites to be used like visible sprites
    Also implementing camera movement
    """

    def __init__(self, cam, *args, **kwargs):
        self._x = 0
        self._y = 0
        self.sprite: Optional[pyglet.sprite.Sprite] = None
        self.cam = cam
        self.cam.on_screen_objects.append(self)
        self._x = kwargs["x"]
        self._y = kwargs["y"]
        if "img" in kwargs:
            self.sprite = pyglet.sprite.Sprite(*args, **kwargs)
        if self.sprite:
            self._height = self.sprite.height
            self._width = self.sprite.width
            self._visible = self.sprite.visible
            # self._x, self._y = self.sprite.position  # todo questionable, may not be updated
            self.set_screen_pos()
        else:
            self._height = kwargs["height"]
            self._width = kwargs["width"]
            self._visible = kwargs["visible"]
            self.set_screen_pos()

    @property
    def x(self):
        return self._x

    @property
    def ax(self):
        return self._x - self.cam.x

    @property
    def y(self):
        return self._y

    @property
    def ay(self):
        return self._y - self.cam.y

    @property
    def height(self):
        return self._height

    @property
    def width(self):
        return self._width

    @property
    def visible(self):
        return self._visible

    @property
    def position(self):
        return self._x, self._y

    @x.setter
    def x(self, new_x):
        self._x = new_x
        self.set_screen_pos()

    @y.setter
    def y(self, new_y):
        self._y = new_y
        self.set_screen_pos()

    @position.setter
    def position(self, pos: tuple):
        self._x, self._y = pos
        self.set_screen_pos()

    @visible.setter
    def visible(self, vis: bool):
        self.visible = vis
        if self.sprite:
            self.sprite.visible = vis

    def set_screen_pos(self):
        if self.sprite:
            self.sprite.x, self.sprite.y = self.cam.get_screen_pos(self._x, self._y)

    def set_intern_pos(self):
        self._x, self._y = self.cam.get_intern_pos(self.sprite.x, self.sprite.y)


class TextQueue(object):
    """Manages a single queue on a single eg. Hotspot or Action"""

    class QueueObject(object):
        """Self-managing Text object"""

        # noinspection PyUnusedLocal
        def __init__(self, text: str, cam, time_: float=2.0, position=None, color=None, target=None, *args, **kwargs) -> None:
            """
            Create a text with the specified attributes, can link back to the player
            :param text: The text to print on the screen, won't be translated
            :param cam:
            :type cam: Camera
            :param time_: Time in seconds how long teh text should stay
            :param position: Where to show the text
            :type position: tuple
            :param color:
            :param target: The player that says the text, used to start the speech animation
            :type target: Player
            """
            assert not (((position and color) or position) and target), "Color/position and target are mutually exclusive"
            self.text = text
            if target:
                position = target.text_pos
                color = target.text_color
            self.on_screen_position = Position(*cam.get_screen_pos(*position))
            self.position = position
            self.time = time_
            self.cam = cam
            self.batch = None
            self.color = color

            self.label = None

        def timeout(self, dt):
            self.time -= dt

        def on_remove(self):
            if self.label:
                self.cam.on_screen_objects.remove(self)
                self.label.delete()

        def on_activate(self, batch):
            self.on_screen_position = Position(*self.cam.get_screen_pos(*self.position))
            self.label = pyglet.text.HTMLLabel(self.text, x=self.on_screen_position.x, y=self.on_screen_position.y,
                                               anchor_x='center', batch=batch)
            self.batch = batch
            self.cam.on_screen_objects.append(self)

        def set_screen_pos(self):
            self.on_screen_position = Position(*self.cam.get_screen_pos(*self.position))
            if self.label:
                self.label.delete()
                self.label = pyglet.text.HTMLLabel(self.text, x=self.on_screen_position.x, y=self.on_screen_position.y,
                                                   anchor_x='center', batch=self.batch)

    def __init__(self, batch):
        self.queue = []
        self.batch = batch

    def say(self, *args, **kwargs):
        self.queue.append(self.QueueObject(*args, **kwargs))
        if len(self.queue) == 1:
            self.queue[0].on_activate(self.batch)

    def tick(self, dt):
        if self.queue:
            self.queue[0].timeout(dt)
            if self.queue[0].time <= 0:
                elem = self.queue.pop(0)
                elem.on_remove()
                if self.queue:
                    self.queue[0].on_activate(self.batch)

            else:
                self.queue[0].time -= dt

    def skip(self, amount: int = 1):
        if not self.queue:
            pass

        elif self.queue:
            self.queue[0].on_remove()
            del self.queue[:amount]
            if self.queue:
                self.queue[0].on_activate(self.batch)


class QueueManager(object):
    """Manages all queues present and provides functions to check for full queues"""

    def __init__(self):
        self.queues = []

    def register_queue(self, q: TextQueue):
        self.queues.append(q)

    def deregister_queue(self, q: TextQueue):
        self.queues.remove(q)

    def get_full(self):
        """
        Returns True if at least one queue is not empty
        :rtype: bool
        """

        return any(x for x in self.queues if x.queue)

    def skip(self, amount=1):
        for queue in self.queues:
            queue.skip(amount)


class PlayerManager(object):
    """Manages all players present, mainly used to give 'all' players to a script"""

    def __init__(self):
        self.players = {}

    def register_player(self, p, lookup_name):
        self.players[lookup_name] = p
        setattr(self, f"p_{lookup_name}", p)

    def deregister_player(self, lookup_name):
        del self.players[lookup_name]
        delattr(self, lookup_name)

    def get_player(self, lookup_name):
        return self.players[lookup_name]

    def __getitem__(self, item):
        return self.players[item]


def animation_from_file_list(files, frame_duration):
    """
    :param files: list of strings containing paths to images
    :param frame_duration: time each frame is visible
    :return: a pyglet Sprite
    """
    return Animation(
        [AnimationFrame(pyglet.image.load(os.path.basename(path), file=load_file(path)), frame_duration) for path in
         files])


def load_file(path):
    """
    Load a file from disk in debug mode and from OFS in production mode
    :param path:
    :return:
    """
    if ofs:
        return ofs.get_file(path)
    else:
        return open(f"./{path}", "rb")


def load_image(path):
    """
    Simplifying image loading
    :param path: the path to the image
    :return: the pyglet sprite
    """
    return pyglet.image.load(os.path.basename(path), file=load_file(path))


def load_config(path):
    return json.load(load_file(path))


def init_discord_rpc():
    """
    Initiate connection with a running discord instance
    :return: None
    """
    config = load_config("data/config/nengine.json")["discord_rpc"]

    presence = pypresence.Presence(config["client_id"])
    try:
        presence.connect()
    except Exception as error:
        print("Could not start Discord Rich Presence, is discord running?", file=sys.stderr)
        print("\t" + str(error), file=sys.stderr)
        return
    if NGN_DEBUG:
        presence.update(os.getpid(), "In the menu", "Idling", start=int(time.time()), large_image=config["large_image"],
                        small_image=config["development_image"], small_text="Development build")
    else:
        presence.update(os.getpid(), "In the menu", "Idling", start=int(time.time()), large_image=config["large_image"])


def point_collision(on_screen_object: OnScreenObject, point_x, point_y):
    """
    Very simple collision checker
    :param on_screen_object: the sprite
    :param point_x: coordinates of the point that is to be checked
    :param point_y: ^
    :rtype: bool
    """
    return on_screen_object.ay < point_y < on_screen_object.ay + on_screen_object.height and \
           on_screen_object.ax < point_x < on_screen_object.ax + on_screen_object.width


def find_class_definition(code, source, class_: type, extend_namespace) -> list:
    """
    Go through a namespace and search for a class definition, that is a type
    :param code: Code to be exec'ed
    :param class_: the type to be searched for
    :param extend_namespace: dict extending the namespace the code is run in
    :return:
    """
    # setting up the namespace the script is going to use
    exec_namespace = {class_.__name__: class_}
    exec_namespace.update(extend_namespace)

    # execute the script in the almost empty namespace defined above
    exec(compile(code, source or None, "exec"), exec_namespace)
    found_class = None

    # search the class defined in the script
    for key, val in exec_namespace.items():

        # test for uninitialized classes
        if type(val) == type:

            # test for subclasses of the class AND exclude the base class
            if issubclass(val, class_) and val != class_:
                if found_class:
                    # already one class defined
                    raise TypeError("More than one class in script")
                found_class = [key, val]

    if not found_class:
        raise TypeError("No class defined in script")

    return found_class


class Position(object):
    """
    Simple object allowing quick math with positions
    """
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def as_tuple(self):
        return self.x, self.y

    def copy(self):
        return Position(self.x, self.y)

    def __add__(self, other):
        if isinstance(other, Position):
            return Position(self.x + other.x, self.y + other.y)

        else:
            return NotImplemented

    def __sub__(self, other):
        if isinstance(other, Position):
            return Position(self.x - other.x, self.y - other.y)

        else:
            return NotImplemented

    def __mul__(self, other):
        if isinstance(other, Position):
            return Position(self.x * other.x, self.y * other.y)

        elif type(other) in (int, float):
            return Position(self.x * other, self.y * other)

        else:
            return NotImplemented

    def __div__(self, other):
        if isinstance(other, Position):
            return Position(self.x / other.x, self.y / other.y)

        elif type(other) in (int, float):
            return Position(self.x / other, self.y / other)

        else:
            return NotImplemented

    def __truediv__(self, other):
        return self.__div__(other)

    def __neg__(self):
        return Position(-self.x, -self.y)

    def __repr__(self):
        return "<Position at {x}, {y}>".format(x=self.x, y=self.y)

    def __iter__(self):
        yield from [self.x, self.y]


class Player(OnScreenObject):
    """ The player representation """

    def __init__(self, cam, animation, speed, anim_speed, *args, **kwargs):
        kwargs["img"] = animation_from_file_list(animation, anim_speed)
        super(Player, self).__init__(cam, *args, **kwargs)
        self.walk_to_position: Optional[Position] = None  # initializing with None indicating no movement
        self.speed = speed

    @classmethod
    def from_config(cls, config, cam):
        return cls(cam, config["animation"], config["speed"], 0.7)

    def walk_to_intern_pos(self, x, y):
        self.walk_to_position = Position(x - self._width / 2, y)

    def walk_to_screen_pos(self, x, y):
        self.walk_to_position = Position(*self.cam.get_intern_pos(x - self._width / 2, y))

    def stop(self):
        self.walk_to_position = Position(*self.position)

    @property
    def text_pos(self):
        return Position(self._x + self._width / 2, self._y + self._height)

    @property
    def text_color(self):
        return [255, 255, 255, 255]  # todo

    # noinspection PyMethodOverriding
    def update(self, dt):
        if self.walk_to_position:
            delta_x = self.walk_to_position.x - self._x
            delta_y = self.walk_to_position.y - self._y

            # if the delta is smaller than the movement, discard it

            if abs(delta_x) < self.speed * dt:
                delta_x = 0

            if abs(delta_y) < self.speed * dt:
                delta_y = 0

            if delta_x == 0 and delta_y == 0:
                self.walk_to_position = None
                return

            # move the player

            if delta_x > 0:
                self.x += self.speed * dt

            elif delta_x < 0:
                self.x -= self.speed * dt

            if delta_y > 0:
                self.y += self.speed * dt

            elif delta_y < 0:
                self.y -= self.speed * dt


class NgnNotYetInitializedError(Exception):
    ...


class Savegame(object):
    filename = "ngnsave"

    def __init__(self):
        ...
